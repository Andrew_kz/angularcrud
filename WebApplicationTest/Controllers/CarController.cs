﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplicationTest.Models;

namespace WebApplicationTest.Controllers
{
    public class CarController : ApiController
    {
        CarContext db = new CarContext();


        [HttpGet]
        [Route("api/getcars")]
        public IHttpActionResult Get()
        {
            var cars = db.Cars.ToList();
            return Json(cars);
        }

        [HttpPost]
        [Route("api/addcar")]
        public IHttpActionResult Post(Car car)
        {
            if (car == null)
            {
                return BadRequest();
            }

            db.Cars.Add(car);
            db.SaveChanges();
            return Ok();
        }

        [HttpPut]
        [Route("api/editcar")]
        public IHttpActionResult Put(Car car)
        {
            if (car == null)
            {
                return BadRequest();
            }

            if (!db.Cars.Any(x => x.Id == car.Id))
            {
                return BadRequest();
            }
            db.Entry(car).State = EntityState.Modified;
            db.SaveChanges();
            return Ok();
        }

        [HttpDelete]
        [Route("api/deletecar")]
        public IHttpActionResult Delete(Car deletedCar)
        {
            Car car = db.Cars.FirstOrDefault(x => x.Id == deletedCar.Id);
            if (car == null)
            {
                return BadRequest();
            }

            db.Cars.Remove(car);
            db.SaveChanges();
            return Ok();
        }

        [HttpDelete]
        [Route("api/deletecar")]
        public IHttpActionResult Delete(int id)
        {
            Car car = db.Cars.FirstOrDefault(x => x.Id == id);
            if (car == null)
            {
                return BadRequest();
            }

            db.Cars.Remove(car);
            db.SaveChanges();
            return Ok();
        }
    }
}
