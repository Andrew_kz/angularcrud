﻿var myApp = angular.module('myApp');
myApp.controller('carController', function ($scope, $http) {
    $scope.CarList = {};
    $scope.Car = {
        Model: '',
        Name: '',
        Category: ''
    }
    $scope.editedCar = {}
    $scope.mode = 'add'


    $scope.getCars = function () {
        $http.get('https://localhost:44311/api/getcars').then(function success(response) {
            $scope.CarList = response.data;
        })
    };

    $scope.deleteCar = function (id) {
        $http({ method: 'POST', url: 'https://localhost:44311/api/deletecar', params: { 'id': id } }).
            then(function success(response) {
                $scope.getCars();
                console.log('deleted');
            })
    };

    $scope.addCar = function (Car) {
        $http({
            method: 'POST', url: 'https://localhost:44311/api/addcar', dataType: 'json', data: JSON.stringify(Car), headers: {
                "Content-Type": "application/json"
            }
        }).
            then(function success(response) {
                $scope.getCars();
                console.log('added');
            })
    };

    $scope.editCar = function (editedCar) {
        $http({
            method: 'PUT', url: 'https://localhost:44311/api/editcar', dataType: 'json', data: JSON.stringify(editedCar), headers: {
                "Content-Type": "application/json"
            }
        }).
            then(function success(response) {
                $scope.getCars();
                $scope.mode = 'add';
                console.log('edited');
            })
    };

    $scope.edit = function (item) {
        $scope.mode = 'edit';
        console.log(item);
        $scope.editedCar = item;
    }
})
