﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplicationTest.Models
{
    public class Car
    {
        public int Id { set; get; }
        public string Model { set; get; }
        public string Name { set; get; }
        public string Category { set; get; }
    }
}